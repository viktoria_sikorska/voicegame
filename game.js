import {Game, Action, ModifyOnce, Voice, Win} from './framework'

const inventory = {
    coins: 0,
}
const start_room = 'room1'
const maze = {
    room1: {
        enter: `Ви хочете моркви, вперед!.`,
        picture: require('./location_images/welcome.svg'),
        actions: {
            north: 'room2',
        },
    },
    room2: {
        enter: `Ти зголоднів. Куди направимося?.`,
        picture: require('./location_images/hare.svg'),
        actions: {
            north: 'room3',
            east: 'room4'
        },
    },
    room3: {
        enter: `Ви потрапили в поле і наїлися моркви!`,
        picture: require('./location_images/кролик_з_морквою.png'),
        actions: {
            north: 'room4',
            east: 'room5',
        },
    },
    room4: {
        picture: require('./location_images/сумний_кролик.png'),
        enter: `З'явився власник поля і вигнав вас`,
        actions: {
            south: 'room3',
        },
    },
    room5: {
        picture: require('./location_images/морква.png'),
        enter: new Win(player => `Ви з'їли ${player.coins} морквин`),
    },
}
const actions = {
    north: ['північ', 'піти на північ'],
    south: ['південь', 'піти на південь'],
    east: ['схід', 'піти на схід'],
    west: ['захід', 'піти на захід'],
    open: ['відкрити', 'відкрити скриню'],
}

new Game({maze, actions, inventory, start_room}).attach(document.body)
